import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:dio/dio.dart';
import './models/interested.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
        // This makes the visual density adapt to the platform that you run
        // the app on. For desktop platforms, the controls will be smaller and
        // closer together (more dense) than on mobile platforms.
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: 'Flutter NTQ Tutorial'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  List<InterestedModel> array = [];

  void getData() async {
    String htoken =
        "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdWQiOlsiYXBwIl0sImlzcyI6ImdvbGYtc2Fsb24iLCJzdWIiOjUzMTYwNzY4NTgwMDM5NDc1MjEsImVtYWlsIjoiYm9va2luZzAxQGdtYWlsLmNvbSIsImN1cnJlbnRSb2xlIjoiR09MRl9QUk8iLCJpYXQiOjE2MTYxMTk0MTUsImV4cCI6MTYxNjcyNDIxNX0.yGb4_mczRkOK823_sCzyBQ9FcngeIUbgjNeLq2uKzLk";
    try {
      Dio dio = Dio();
      Response response = await dio.get(
          "https://api-golfsalon-dev.ntq.solutions/v2/app/interested",
          options: Options(
            headers: {"Authorization": "$htoken"},
          ));

      var a = response.data
          .map((element) => InterestedModel.formJson(element))
          ?.toList()
          ?.cast<InterestedModel>();
      this.setState(() {
        this.array = a;
      });
    } on DioError catch (e) {
      print(e.response.data);
      print(e.response.headers);
      print(e.response.request);
    }
  }

  @override
  initState() {
    super.initState();
    // Add listeners to this class
    getData();
  }

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
        appBar: AppBar(
            // Here we take the value from the MyHomePage object that was created by
            // the App.build method, and use it to set our appbar title.
            ),
        body: Center(
          child: ListView.separated(
            padding: const EdgeInsets.all(8),
            itemCount: array.length,
            itemBuilder: (BuildContext context, int index) {
              return Container(
                height: 50,
                child: Center(child: Text('${array[index].interested}')),
              );
            },
            separatorBuilder: (BuildContext context, int index) =>
                const Divider(),
          ),
          // Center is a layout widget. It takes a single child and positions it
          // in the middle of the parent
        ) // This trailing comma makes auto-formatting nicer for build methods.
        );
  }
}
