class InterestedModel {
  String id;
  String interested;

  static InterestedModel formJson(Map<String, dynamic> json) {
    return InterestedModel()
      ..id = json["id"]
      ..interested = json["interested_name"];
  }
}
